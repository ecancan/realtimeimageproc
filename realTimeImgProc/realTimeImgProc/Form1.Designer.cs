﻿namespace realTimeImgProc
{
    partial class Form1
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dosyaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kaydetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.işlemlerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.görüntüyüAynalaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projelerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proje1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proje2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proje3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kameraÇözünürlüğüToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vGAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mP1280x960ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mP1600x1200ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mP2048x1536ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mP2240x1680ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mP2560x1920ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mP3032x2008ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mP3072x2304ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mP3264x2448ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sourceBox = new System.Windows.Forms.PictureBox();
            this.procBox = new System.Windows.Forms.PictureBox();
            this.start = new System.Windows.Forms.Button();
            this.stop = new System.Windows.Forms.Button();
            this.capture = new System.Windows.Forms.Button();
            this.deviceSelect = new System.Windows.Forms.ComboBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.grayscale = new System.Windows.Forms.RadioButton();
            this.otsu = new System.Windows.Forms.RadioButton();
            this.colorTarget = new System.Windows.Forms.RadioButton();
            this.redTrack = new System.Windows.Forms.TrackBar();
            this.greenTrack = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.detectCoor = new System.Windows.Forms.CheckBox();
            this.blueTrack = new System.Windows.Forms.TrackBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.detectDevices = new System.Windows.Forms.Button();
            this.disconnectSerial = new System.Windows.Forms.Button();
            this.connectSerial = new System.Windows.Forms.Button();
            this.connectTrack = new System.Windows.Forms.ProgressBar();
            this.selectBaudRate = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.selectPort = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.writeCoordinate = new System.Windows.Forms.RichTextBox();
            this.ledinterface = new System.Windows.Forms.GroupBox();
            this.obje2Coor = new System.Windows.Forms.RichTextBox();
            this.ardiunoProject1 = new System.Windows.Forms.Button();
            this.ardiunoProject2 = new System.Windows.Forms.Button();
            this.ardiunoProject3 = new System.Windows.Forms.Button();
            this.projeStatusViewer = new System.Windows.Forms.Label();
            this.resolutionView = new System.Windows.Forms.Label();
            this.obje2 = new System.Windows.Forms.GroupBox();
            this.middleArea = new System.Windows.Forms.GroupBox();
            this.areaValueView = new System.Windows.Forms.Label();
            this.areaTrackBar = new System.Windows.Forms.TrackBar();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sourceBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.procBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.redTrack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenTrack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blueTrack)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.ledinterface.SuspendLayout();
            this.obje2.SuspendLayout();
            this.middleArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.areaTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dosyaToolStripMenuItem,
            this.işlemlerToolStripMenuItem,
            this.projelerToolStripMenuItem,
            this.kameraÇözünürlüğüToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1290, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dosyaToolStripMenuItem
            // 
            this.dosyaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kaydetToolStripMenuItem});
            this.dosyaToolStripMenuItem.Name = "dosyaToolStripMenuItem";
            this.dosyaToolStripMenuItem.Size = new System.Drawing.Size(62, 24);
            this.dosyaToolStripMenuItem.Text = "Dosya";
            // 
            // kaydetToolStripMenuItem
            // 
            this.kaydetToolStripMenuItem.Name = "kaydetToolStripMenuItem";
            this.kaydetToolStripMenuItem.Size = new System.Drawing.Size(130, 26);
            this.kaydetToolStripMenuItem.Text = "Kaydet";
            this.kaydetToolStripMenuItem.Click += new System.EventHandler(this.kaydetToolStripMenuItem_Click);
            // 
            // işlemlerToolStripMenuItem
            // 
            this.işlemlerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.görüntüyüAynalaToolStripMenuItem});
            this.işlemlerToolStripMenuItem.Name = "işlemlerToolStripMenuItem";
            this.işlemlerToolStripMenuItem.Size = new System.Drawing.Size(73, 24);
            this.işlemlerToolStripMenuItem.Text = "İşlemler";
            // 
            // görüntüyüAynalaToolStripMenuItem
            // 
            this.görüntüyüAynalaToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.görüntüyüAynalaToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.görüntüyüAynalaToolStripMenuItem.Name = "görüntüyüAynalaToolStripMenuItem";
            this.görüntüyüAynalaToolStripMenuItem.Size = new System.Drawing.Size(201, 26);
            this.görüntüyüAynalaToolStripMenuItem.Text = "Görüntüyü Aynala";
            this.görüntüyüAynalaToolStripMenuItem.Click += new System.EventHandler(this.görüntüyüAynalaToolStripMenuItem_Click);
            // 
            // projelerToolStripMenuItem
            // 
            this.projelerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.proje1ToolStripMenuItem,
            this.proje2ToolStripMenuItem,
            this.proje3ToolStripMenuItem});
            this.projelerToolStripMenuItem.Name = "projelerToolStripMenuItem";
            this.projelerToolStripMenuItem.Size = new System.Drawing.Size(72, 24);
            this.projelerToolStripMenuItem.Text = "Projeler";
            // 
            // proje1ToolStripMenuItem
            // 
            this.proje1ToolStripMenuItem.Name = "proje1ToolStripMenuItem";
            this.proje1ToolStripMenuItem.Size = new System.Drawing.Size(130, 26);
            this.proje1ToolStripMenuItem.Text = "Proje 1";
            this.proje1ToolStripMenuItem.Click += new System.EventHandler(this.proje1ToolStripMenuItem_Click);
            // 
            // proje2ToolStripMenuItem
            // 
            this.proje2ToolStripMenuItem.Name = "proje2ToolStripMenuItem";
            this.proje2ToolStripMenuItem.Size = new System.Drawing.Size(130, 26);
            this.proje2ToolStripMenuItem.Text = "Proje 2";
            this.proje2ToolStripMenuItem.Click += new System.EventHandler(this.proje2ToolStripMenuItem_Click);
            // 
            // proje3ToolStripMenuItem
            // 
            this.proje3ToolStripMenuItem.Name = "proje3ToolStripMenuItem";
            this.proje3ToolStripMenuItem.Size = new System.Drawing.Size(130, 26);
            this.proje3ToolStripMenuItem.Text = "Proje 3";
            this.proje3ToolStripMenuItem.Click += new System.EventHandler(this.proje3ToolStripMenuItem_Click);
            // 
            // kameraÇözünürlüğüToolStripMenuItem
            // 
            this.kameraÇözünürlüğüToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vGAToolStripMenuItem,
            this.mP1280x960ToolStripMenuItem,
            this.mP1600x1200ToolStripMenuItem,
            this.mP2048x1536ToolStripMenuItem,
            this.mP2240x1680ToolStripMenuItem,
            this.mP2560x1920ToolStripMenuItem,
            this.mP3032x2008ToolStripMenuItem,
            this.mP3072x2304ToolStripMenuItem,
            this.mP3264x2448ToolStripMenuItem});
            this.kameraÇözünürlüğüToolStripMenuItem.Name = "kameraÇözünürlüğüToolStripMenuItem";
            this.kameraÇözünürlüğüToolStripMenuItem.Size = new System.Drawing.Size(159, 24);
            this.kameraÇözünürlüğüToolStripMenuItem.Text = "Kamera Çözünürlüğü";
            // 
            // vGAToolStripMenuItem
            // 
            this.vGAToolStripMenuItem.Name = "vGAToolStripMenuItem";
            this.vGAToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.vGAToolStripMenuItem.Text = "0.3 MP - 640x480";
            this.vGAToolStripMenuItem.Click += new System.EventHandler(this.vGAToolStripMenuItem_Click);
            // 
            // mP1280x960ToolStripMenuItem
            // 
            this.mP1280x960ToolStripMenuItem.Name = "mP1280x960ToolStripMenuItem";
            this.mP1280x960ToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.mP1280x960ToolStripMenuItem.Text = "1 MP - 1280x960";
            this.mP1280x960ToolStripMenuItem.Click += new System.EventHandler(this.mP1280x960ToolStripMenuItem_Click);
            // 
            // mP1600x1200ToolStripMenuItem
            // 
            this.mP1600x1200ToolStripMenuItem.Name = "mP1600x1200ToolStripMenuItem";
            this.mP1600x1200ToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.mP1600x1200ToolStripMenuItem.Text = "2 MP - 1600x1200";
            this.mP1600x1200ToolStripMenuItem.Click += new System.EventHandler(this.mP1600x1200ToolStripMenuItem_Click);
            // 
            // mP2048x1536ToolStripMenuItem
            // 
            this.mP2048x1536ToolStripMenuItem.Name = "mP2048x1536ToolStripMenuItem";
            this.mP2048x1536ToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.mP2048x1536ToolStripMenuItem.Text = "3 MP - 2048x1536";
            this.mP2048x1536ToolStripMenuItem.Click += new System.EventHandler(this.mP2048x1536ToolStripMenuItem_Click);
            // 
            // mP2240x1680ToolStripMenuItem
            // 
            this.mP2240x1680ToolStripMenuItem.Name = "mP2240x1680ToolStripMenuItem";
            this.mP2240x1680ToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.mP2240x1680ToolStripMenuItem.Text = "4 MP - 2240x1680";
            this.mP2240x1680ToolStripMenuItem.Click += new System.EventHandler(this.mP2240x1680ToolStripMenuItem_Click);
            // 
            // mP2560x1920ToolStripMenuItem
            // 
            this.mP2560x1920ToolStripMenuItem.Name = "mP2560x1920ToolStripMenuItem";
            this.mP2560x1920ToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.mP2560x1920ToolStripMenuItem.Text = "5 MP - 2560x1920";
            this.mP2560x1920ToolStripMenuItem.Click += new System.EventHandler(this.mP2560x1920ToolStripMenuItem_Click);
            // 
            // mP3032x2008ToolStripMenuItem
            // 
            this.mP3032x2008ToolStripMenuItem.Name = "mP3032x2008ToolStripMenuItem";
            this.mP3032x2008ToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.mP3032x2008ToolStripMenuItem.Text = "6 MP - 3032x2008";
            this.mP3032x2008ToolStripMenuItem.Click += new System.EventHandler(this.mP3032x2008ToolStripMenuItem_Click);
            // 
            // mP3072x2304ToolStripMenuItem
            // 
            this.mP3072x2304ToolStripMenuItem.Name = "mP3072x2304ToolStripMenuItem";
            this.mP3072x2304ToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.mP3072x2304ToolStripMenuItem.Text = "7 MP - 3072x2304";
            this.mP3072x2304ToolStripMenuItem.Click += new System.EventHandler(this.mP3072x2304ToolStripMenuItem_Click);
            // 
            // mP3264x2448ToolStripMenuItem
            // 
            this.mP3264x2448ToolStripMenuItem.Name = "mP3264x2448ToolStripMenuItem";
            this.mP3264x2448ToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.mP3264x2448ToolStripMenuItem.Text = "8 MP - 3264x2448";
            this.mP3264x2448ToolStripMenuItem.Click += new System.EventHandler(this.mP3264x2448ToolStripMenuItem_Click);
            // 
            // sourceBox
            // 
            this.sourceBox.Image = ((System.Drawing.Image)(resources.GetObject("sourceBox.Image")));
            this.sourceBox.Location = new System.Drawing.Point(12, 40);
            this.sourceBox.Name = "sourceBox";
            this.sourceBox.Size = new System.Drawing.Size(400, 300);
            this.sourceBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.sourceBox.TabIndex = 1;
            this.sourceBox.TabStop = false;
            // 
            // procBox
            // 
            this.procBox.Image = ((System.Drawing.Image)(resources.GetObject("procBox.Image")));
            this.procBox.Location = new System.Drawing.Point(435, 40);
            this.procBox.Name = "procBox";
            this.procBox.Size = new System.Drawing.Size(400, 300);
            this.procBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.procBox.TabIndex = 2;
            this.procBox.TabStop = false;
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(435, 360);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(144, 39);
            this.start.TabIndex = 3;
            this.start.Text = "Başlat";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // stop
            // 
            this.stop.Location = new System.Drawing.Point(585, 360);
            this.stop.Name = "stop";
            this.stop.Size = new System.Drawing.Size(121, 39);
            this.stop.TabIndex = 4;
            this.stop.Text = "Durdur";
            this.stop.UseVisualStyleBackColor = true;
            this.stop.Click += new System.EventHandler(this.stop_Click);
            // 
            // capture
            // 
            this.capture.Location = new System.Drawing.Point(712, 360);
            this.capture.Name = "capture";
            this.capture.Size = new System.Drawing.Size(123, 39);
            this.capture.TabIndex = 5;
            this.capture.Text = "Ekranı Yakala";
            this.capture.UseVisualStyleBackColor = true;
            this.capture.Click += new System.EventHandler(this.capture_Click);
            // 
            // deviceSelect
            // 
            this.deviceSelect.FormattingEnabled = true;
            this.deviceSelect.Location = new System.Drawing.Point(12, 368);
            this.deviceSelect.Name = "deviceSelect";
            this.deviceSelect.Size = new System.Drawing.Size(400, 24);
            this.deviceSelect.TabIndex = 6;
            // 
            // grayscale
            // 
            this.grayscale.AutoSize = true;
            this.grayscale.Checked = true;
            this.grayscale.Location = new System.Drawing.Point(861, 40);
            this.grayscale.Name = "grayscale";
            this.grayscale.Size = new System.Drawing.Size(93, 21);
            this.grayscale.TabIndex = 7;
            this.grayscale.TabStop = true;
            this.grayscale.Text = "Grayscale";
            this.grayscale.UseVisualStyleBackColor = true;
            // 
            // otsu
            // 
            this.otsu.AutoSize = true;
            this.otsu.Location = new System.Drawing.Point(977, 40);
            this.otsu.Name = "otsu";
            this.otsu.Size = new System.Drawing.Size(59, 21);
            this.otsu.TabIndex = 8;
            this.otsu.Text = "Otsu";
            this.otsu.UseVisualStyleBackColor = true;
            // 
            // colorTarget
            // 
            this.colorTarget.AutoSize = true;
            this.colorTarget.Location = new System.Drawing.Point(1070, 40);
            this.colorTarget.Name = "colorTarget";
            this.colorTarget.Size = new System.Drawing.Size(134, 21);
            this.colorTarget.TabIndex = 9;
            this.colorTarget.Text = "Renk Hedefleme";
            this.colorTarget.UseVisualStyleBackColor = true;
            // 
            // redTrack
            // 
            this.redTrack.Location = new System.Drawing.Point(861, 67);
            this.redTrack.Maximum = 255;
            this.redTrack.Name = "redTrack";
            this.redTrack.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.redTrack.Size = new System.Drawing.Size(56, 301);
            this.redTrack.TabIndex = 10;
            this.redTrack.Scroll += new System.EventHandler(this.redTrack_Scroll);
            // 
            // greenTrack
            // 
            this.greenTrack.Location = new System.Drawing.Point(937, 67);
            this.greenTrack.Maximum = 255;
            this.greenTrack.Name = "greenTrack";
            this.greenTrack.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.greenTrack.Size = new System.Drawing.Size(56, 301);
            this.greenTrack.TabIndex = 11;
            this.greenTrack.Scroll += new System.EventHandler(this.greenTrack_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(861, 375);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = "R";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(938, 375);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "G";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1010, 375);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 17);
            this.label3.TabIndex = 15;
            this.label3.Text = "B";
            // 
            // detectCoor
            // 
            this.detectCoor.AutoSize = true;
            this.detectCoor.Location = new System.Drawing.Point(1070, 72);
            this.detectCoor.Name = "detectCoor";
            this.detectCoor.Size = new System.Drawing.Size(118, 21);
            this.detectCoor.TabIndex = 17;
            this.detectCoor.Text = "Kordinatları Al";
            this.detectCoor.UseVisualStyleBackColor = true;
            // 
            // blueTrack
            // 
            this.blueTrack.Location = new System.Drawing.Point(999, 67);
            this.blueTrack.Maximum = 255;
            this.blueTrack.Name = "blueTrack";
            this.blueTrack.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.blueTrack.Size = new System.Drawing.Size(56, 301);
            this.blueTrack.TabIndex = 18;
            this.blueTrack.Scroll += new System.EventHandler(this.blueTrack_Scroll_1);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.Location = new System.Drawing.Point(11, 21);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(59, 53);
            this.panel1.TabIndex = 19;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel2.Location = new System.Drawing.Point(76, 21);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(59, 53);
            this.panel2.TabIndex = 20;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel3.Location = new System.Drawing.Point(141, 21);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(59, 53);
            this.panel3.TabIndex = 21;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel4.Location = new System.Drawing.Point(11, 80);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(59, 53);
            this.panel4.TabIndex = 22;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel5.Location = new System.Drawing.Point(76, 80);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(59, 53);
            this.panel5.TabIndex = 23;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel6.Location = new System.Drawing.Point(141, 80);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(59, 53);
            this.panel6.TabIndex = 24;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel7.Location = new System.Drawing.Point(11, 139);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(59, 53);
            this.panel7.TabIndex = 27;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel8.Location = new System.Drawing.Point(76, 139);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(59, 53);
            this.panel8.TabIndex = 26;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel9.Location = new System.Drawing.Point(141, 139);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(59, 53);
            this.panel9.TabIndex = 25;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.detectDevices);
            this.groupBox1.Controls.Add(this.disconnectSerial);
            this.groupBox1.Controls.Add(this.connectSerial);
            this.groupBox1.Controls.Add(this.connectTrack);
            this.groupBox1.Controls.Add(this.selectBaudRate);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.selectPort);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 412);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1269, 76);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Arduino Bağlantısı (Seri Port Bağlantısı)";
            // 
            // detectDevices
            // 
            this.detectDevices.Location = new System.Drawing.Point(815, 30);
            this.detectDevices.Name = "detectDevices";
            this.detectDevices.Size = new System.Drawing.Size(144, 25);
            this.detectDevices.TabIndex = 31;
            this.detectDevices.Text = "Cihazları Bul";
            this.detectDevices.UseVisualStyleBackColor = true;
            this.detectDevices.Click += new System.EventHandler(this.detectDevices_Click);
            // 
            // disconnectSerial
            // 
            this.disconnectSerial.Location = new System.Drawing.Point(1114, 30);
            this.disconnectSerial.Name = "disconnectSerial";
            this.disconnectSerial.Size = new System.Drawing.Size(144, 24);
            this.disconnectSerial.TabIndex = 30;
            this.disconnectSerial.Text = "Bağlantıyı Kes";
            this.disconnectSerial.UseVisualStyleBackColor = true;
            this.disconnectSerial.Click += new System.EventHandler(this.disconnectSerial_Click);
            // 
            // connectSerial
            // 
            this.connectSerial.Location = new System.Drawing.Point(965, 31);
            this.connectSerial.Name = "connectSerial";
            this.connectSerial.Size = new System.Drawing.Size(144, 24);
            this.connectSerial.TabIndex = 29;
            this.connectSerial.Text = "Bağlan";
            this.connectSerial.UseVisualStyleBackColor = true;
            this.connectSerial.Click += new System.EventHandler(this.connectSerial_Click);
            // 
            // connectTrack
            // 
            this.connectTrack.Location = new System.Drawing.Point(546, 31);
            this.connectTrack.Name = "connectTrack";
            this.connectTrack.Size = new System.Drawing.Size(263, 24);
            this.connectTrack.TabIndex = 4;
            // 
            // selectBaudRate
            // 
            this.selectBaudRate.FormattingEnabled = true;
            this.selectBaudRate.Items.AddRange(new object[] {
            "2400",
            "4800",
            "9600",
            "115200"});
            this.selectBaudRate.Location = new System.Drawing.Point(369, 31);
            this.selectBaudRate.Name = "selectBaudRate";
            this.selectBaudRate.Size = new System.Drawing.Size(171, 24);
            this.selectBaudRate.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(288, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "Baud Rate";
            // 
            // selectPort
            // 
            this.selectPort.FormattingEnabled = true;
            this.selectPort.Location = new System.Drawing.Point(93, 31);
            this.selectPort.Name = "selectPort";
            this.selectPort.Size = new System.Drawing.Size(171, 24);
            this.selectPort.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Port Seçimi";
            // 
            // writeCoordinate
            // 
            this.writeCoordinate.Location = new System.Drawing.Point(1070, 100);
            this.writeCoordinate.Name = "writeCoordinate";
            this.writeCoordinate.Size = new System.Drawing.Size(208, 123);
            this.writeCoordinate.TabIndex = 29;
            this.writeCoordinate.Text = "";
            // 
            // ledinterface
            // 
            this.ledinterface.Controls.Add(this.panel1);
            this.ledinterface.Controls.Add(this.panel2);
            this.ledinterface.Controls.Add(this.panel3);
            this.ledinterface.Controls.Add(this.panel9);
            this.ledinterface.Controls.Add(this.panel4);
            this.ledinterface.Controls.Add(this.panel8);
            this.ledinterface.Controls.Add(this.panel7);
            this.ledinterface.Controls.Add(this.panel5);
            this.ledinterface.Controls.Add(this.panel6);
            this.ledinterface.Location = new System.Drawing.Point(1070, 229);
            this.ledinterface.Name = "ledinterface";
            this.ledinterface.Size = new System.Drawing.Size(211, 203);
            this.ledinterface.TabIndex = 30;
            this.ledinterface.TabStop = false;
            this.ledinterface.Text = "Led Arayüzü";
            // 
            // obje2Coor
            // 
            this.obje2Coor.Location = new System.Drawing.Point(0, 19);
            this.obje2Coor.Name = "obje2Coor";
            this.obje2Coor.Size = new System.Drawing.Size(208, 167);
            this.obje2Coor.TabIndex = 32;
            this.obje2Coor.Text = "";
            this.obje2Coor.Visible = false;
            // 
            // ardiunoProject1
            // 
            this.ardiunoProject1.Location = new System.Drawing.Point(12, 494);
            this.ardiunoProject1.Name = "ardiunoProject1";
            this.ardiunoProject1.Size = new System.Drawing.Size(409, 38);
            this.ardiunoProject1.TabIndex = 33;
            this.ardiunoProject1.Text = "Proje 1 İçin Ardiuno Kod Bloğunu Aktif Et";
            this.ardiunoProject1.UseVisualStyleBackColor = true;
            this.ardiunoProject1.Click += new System.EventHandler(this.ardiunoProject1_Click);
            // 
            // ardiunoProject2
            // 
            this.ardiunoProject2.Enabled = false;
            this.ardiunoProject2.Location = new System.Drawing.Point(427, 494);
            this.ardiunoProject2.Name = "ardiunoProject2";
            this.ardiunoProject2.Size = new System.Drawing.Size(414, 38);
            this.ardiunoProject2.TabIndex = 34;
            this.ardiunoProject2.Text = "Proje 2 İçin Ardiuno Kod Bloğunu Aktif Et";
            this.ardiunoProject2.UseVisualStyleBackColor = true;
            this.ardiunoProject2.Click += new System.EventHandler(this.ardiunoProject2_Click);
            // 
            // ardiunoProject3
            // 
            this.ardiunoProject3.Enabled = false;
            this.ardiunoProject3.Location = new System.Drawing.Point(847, 494);
            this.ardiunoProject3.Name = "ardiunoProject3";
            this.ardiunoProject3.Size = new System.Drawing.Size(434, 38);
            this.ardiunoProject3.TabIndex = 35;
            this.ardiunoProject3.Text = "Proje 3 İçin Ardiuno Kod Bloğunu Aktif Et\r\n";
            this.ardiunoProject3.UseVisualStyleBackColor = true;
            this.ardiunoProject3.Click += new System.EventHandler(this.ardiunoProject3_Click);
            // 
            // projeStatusViewer
            // 
            this.projeStatusViewer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(36)))), ((int)(((byte)(50)))));
            this.projeStatusViewer.Font = new System.Drawing.Font("Poppins SemiBold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.projeStatusViewer.ForeColor = System.Drawing.Color.White;
            this.projeStatusViewer.Location = new System.Drawing.Point(1190, 0);
            this.projeStatusViewer.Margin = new System.Windows.Forms.Padding(0);
            this.projeStatusViewer.Name = "projeStatusViewer";
            this.projeStatusViewer.Size = new System.Drawing.Size(100, 28);
            this.projeStatusViewer.TabIndex = 36;
            this.projeStatusViewer.Text = "Proje 1";
            this.projeStatusViewer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // resolutionView
            // 
            this.resolutionView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.resolutionView.Font = new System.Drawing.Font("Poppins SemiBold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.resolutionView.ForeColor = System.Drawing.Color.White;
            this.resolutionView.Location = new System.Drawing.Point(1088, 0);
            this.resolutionView.Margin = new System.Windows.Forms.Padding(0);
            this.resolutionView.Name = "resolutionView";
            this.resolutionView.Size = new System.Drawing.Size(100, 28);
            this.resolutionView.TabIndex = 37;
            this.resolutionView.Text = "640x480";
            this.resolutionView.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // obje2
            // 
            this.obje2.Controls.Add(this.obje2Coor);
            this.obje2.Location = new System.Drawing.Point(1070, 227);
            this.obje2.Name = "obje2";
            this.obje2.Size = new System.Drawing.Size(208, 186);
            this.obje2.TabIndex = 38;
            this.obje2.TabStop = false;
            this.obje2.Text = "2.Cismin Kordinatları";
            // 
            // middleArea
            // 
            this.middleArea.Controls.Add(this.areaValueView);
            this.middleArea.Controls.Add(this.areaTrackBar);
            this.middleArea.Location = new System.Drawing.Point(1070, 230);
            this.middleArea.Name = "middleArea";
            this.middleArea.Size = new System.Drawing.Size(208, 172);
            this.middleArea.TabIndex = 39;
            this.middleArea.TabStop = false;
            this.middleArea.Text = "Hedef Bölge Alanı";
            // 
            // areaValueView
            // 
            this.areaValueView.AutoSize = true;
            this.areaValueView.Font = new System.Drawing.Font("Poppins SemiBold", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.areaValueView.Location = new System.Drawing.Point(73, 78);
            this.areaValueView.Name = "areaValueView";
            this.areaValueView.Size = new System.Drawing.Size(67, 58);
            this.areaValueView.TabIndex = 1;
            this.areaValueView.Text = "60";
            this.areaValueView.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // areaTrackBar
            // 
            this.areaTrackBar.Location = new System.Drawing.Point(6, 21);
            this.areaTrackBar.Maximum = 100;
            this.areaTrackBar.Minimum = 60;
            this.areaTrackBar.Name = "areaTrackBar";
            this.areaTrackBar.Size = new System.Drawing.Size(196, 56);
            this.areaTrackBar.TabIndex = 0;
            this.areaTrackBar.Value = 60;
            this.areaTrackBar.Scroll += new System.EventHandler(this.areaTrackBar_Scroll);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1290, 544);
            this.Controls.Add(this.middleArea);
            this.Controls.Add(this.obje2);
            this.Controls.Add(this.resolutionView);
            this.Controls.Add(this.projeStatusViewer);
            this.Controls.Add(this.ardiunoProject3);
            this.Controls.Add(this.ardiunoProject2);
            this.Controls.Add(this.ardiunoProject1);
            this.Controls.Add(this.ledinterface);
            this.Controls.Add(this.writeCoordinate);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.blueTrack);
            this.Controls.Add(this.detectCoor);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.greenTrack);
            this.Controls.Add(this.redTrack);
            this.Controls.Add(this.colorTarget);
            this.Controls.Add(this.otsu);
            this.Controls.Add(this.grayscale);
            this.Controls.Add(this.deviceSelect);
            this.Controls.Add(this.capture);
            this.Controls.Add(this.stop);
            this.Controls.Add(this.start);
            this.Controls.Add(this.procBox);
            this.Controls.Add(this.sourceBox);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Gerçek Zamanlı Görüntü İşleme Ahmet CAN";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sourceBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.procBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.redTrack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenTrack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blueTrack)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ledinterface.ResumeLayout(false);
            this.obje2.ResumeLayout(false);
            this.middleArea.ResumeLayout(false);
            this.middleArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.areaTrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dosyaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem işlemlerToolStripMenuItem;
        private System.Windows.Forms.PictureBox sourceBox;
        private System.Windows.Forms.PictureBox procBox;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Button stop;
        private System.Windows.Forms.Button capture;
        private System.Windows.Forms.ComboBox deviceSelect;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.RadioButton grayscale;
        private System.Windows.Forms.RadioButton otsu;
        private System.Windows.Forms.RadioButton colorTarget;
        private System.Windows.Forms.TrackBar redTrack;
        private System.Windows.Forms.TrackBar greenTrack;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox detectCoor;
        private System.Windows.Forms.TrackBar blueTrack;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox selectPort;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox selectBaudRate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ProgressBar connectTrack;
        private System.Windows.Forms.Button connectSerial;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Button disconnectSerial;
        private System.Windows.Forms.Button detectDevices;
        private System.Windows.Forms.RichTextBox writeCoordinate;
        private System.Windows.Forms.ToolStripMenuItem görüntüyüAynalaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kaydetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projelerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proje1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proje2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proje3ToolStripMenuItem;
        private System.Windows.Forms.GroupBox ledinterface;
        private System.Windows.Forms.RichTextBox obje2Coor;
        private System.Windows.Forms.Button ardiunoProject1;
        private System.Windows.Forms.Button ardiunoProject2;
        private System.Windows.Forms.Button ardiunoProject3;
        private System.Windows.Forms.Label projeStatusViewer;
        private System.Windows.Forms.ToolStripMenuItem kameraÇözünürlüğüToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vGAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mP1280x960ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mP1600x1200ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mP2048x1536ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mP2240x1680ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mP2560x1920ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mP3032x2008ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mP3072x2304ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mP3264x2448ToolStripMenuItem;
        private System.Windows.Forms.Label resolutionView;
        private System.Windows.Forms.GroupBox obje2;
        private System.Windows.Forms.GroupBox middleArea;
        private System.Windows.Forms.TrackBar areaTrackBar;
        private System.Windows.Forms.Label areaValueView;
    }
}

