﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge;
using AForge.Imaging;
using AForge.Video;
using AForge.Video.DirectShow;
using AForge.Imaging.Filters;
using System.IO.Ports;
using System.Diagnostics;

namespace realTimeImgProc
{
    public partial class Form1 : Form
    {
        int R = 0;
        int G = 0;
        int B = 0;
        int mirroring = 0;
        int cameraRun = 0;
        bool captures = false;
        int objeX;
        int objeY;
        int obje1x = 0, obje1y = 0, obje2x = 0, obje2y = 0;
        int projectStatus = 1;
        string rotation;
        string temp;
        int ledPin;
        int areaValue = 60;
        string cameraResolution = "640x480";
        Bitmap proc;
        // 26.10.2018 Görüntü işleme proje için ilk adım :) Ahmet CAN
        private FilterInfoCollection videoDevices;
        private VideoCaptureDevice camera;
        public Form1()
        {
            InitializeComponent();
            stop.Enabled = false;
            capture.Enabled = false;
            videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (videoDevices.Count != 0) {
                for (int i = 1; i <= videoDevices.Count; i++) {
                    string videoDeviceName = i + " : " + videoDevices[i - 1].Name;
                    deviceSelect.Items.Add(videoDeviceName);
                }
                if (videoDevices.Count == 1)
                {
                    deviceSelect.SelectedIndex = 0;
                    deviceSelect.Enabled = false;
                }
            }
            else
            {
                MessageBox.Show("Herhangi bir görüntü sağlayıcı cihaz bulunamadı.", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void start_Click(object sender, EventArgs e)
        {
            if (deviceSelect.Text != "")
            {
                camera = new VideoCaptureDevice(videoDevices[deviceSelect.SelectedIndex].MonikerString);
                camera.NewFrame += new NewFrameEventHandler(cameraNewFrame);
                camera.DesiredFrameRate = 30;
                camera.DesiredFrameSize = new Size(400, 300);
                camera.Start();
                start.Enabled = false;
                stop.Enabled = true;
                capture.Enabled = true;
                cameraRun = 1;
            }
            else
            {
                MessageBox.Show("Herhangi bir görüntü aygıtı seçmediniz!","Hata",MessageBoxButtons.OK,MessageBoxIcon.Stop);
            }
            

        }
        void cameraNewFrame(object sender, NewFrameEventArgs main)
        {
            Bitmap sourceImage = (Bitmap)main.Frame.Clone();
            Bitmap procImage = (Bitmap)main.Frame.Clone();
            if (mirroring == 1)
            {
                Mirror filter = new Mirror(false, true);
                filter.ApplyInPlace(sourceImage);
                filter.ApplyInPlace(procImage);
            }
            
            sourceBox.Image = sourceImage;
            procFunc(procImage);
            
            
        }
        private void procFunc(Bitmap getParameter)
        {
            if (grayscale.Checked)
            {
                proc = new GrayscaleBT709().Apply(getParameter);
            } else if (otsu.Checked)
            {
                proc = new GrayscaleBT709().Apply(getParameter);
                proc = new OtsuThreshold().Apply(proc);
            } else if (colorTarget.Checked)
            {
                proc = getParameter;
                EuclideanColorFiltering filter = new EuclideanColorFiltering();
                filter.CenterColor = new RGB(Color.FromArgb(R, G, B));
                filter.Radius = 100;
                filter.ApplyInPlace(proc);
                BlobCounter blobcounter = new BlobCounter();
                blobcounter.MinHeight = 20;
                blobcounter.MinWidth = 20;
                blobcounter.ObjectsOrder = ObjectsOrder.Size;
                blobcounter.ProcessImage(proc);
                Rectangle[] rectangleShape = blobcounter.GetObjectsRectangles();

                if (projectStatus == 1)
                {
                    if (rectangleShape.Length > 0)
                    {
                        Rectangle obje = rectangleShape[0];
                        Graphics graphics = Graphics.FromImage(proc);
                        using (Pen border = new Pen(Color.White, 1))
                        {
                            graphics.DrawRectangle(border, obje);
                        }
                        graphics.Dispose();
                        objeX = obje.X + (obje.Width / 2); // belirlediğimiz karenin x eksenindeki ortası
                        objeY = obje.Y + (obje.Height / 2); // belirlediğimiz karenin y eksenindeki ortası

                        if (detectCoor.Checked)
                        {
                            this.Invoke((MethodInvoker)delegate {
                                //writeCoordinate.Text = obje.Location.ToString() + "\n" + writeCoordinate.Text + "\n"; // direkt olarak aforgenin özelliği
                                writeCoordinate.Text = "X: " + objeX.ToString() + " -- Y: " + objeY.ToString() + "\n" + writeCoordinate.Text + "\n"; // manuel olarka yapılan kordinat
                            });
                            divScreenProject1(objeX, objeY);
                            if (serialPort1.IsOpen)
                            {
                                serialPort1.DataReceived += new SerialDataReceivedEventHandler(ReceivedSerialHandler);
                            }
                            
                        }
                    }
                }else if (projectStatus == 2)
                {
                    string[] vs2 = cameraResolution.Split('x');
                    int objeXValue = Convert.ToInt32(vs2[0]);
                    objeXValue = objeXValue / 2;
                    Graphics graphicsCenterDetect = Graphics.FromImage(proc);
                    Pen guidelinesRectangle = new Pen(Color.LightGreen, 3);
                    graphicsCenterDetect.DrawLine(guidelinesRectangle, objeXValue-areaValue, 0, objeXValue - areaValue, Convert.ToInt32(vs2[1]));
                    graphicsCenterDetect.DrawLine(guidelinesRectangle, objeXValue + areaValue, 0, objeXValue + areaValue, Convert.ToInt32(vs2[1]));
                    if (rectangleShape.Length > 0)
                    {
                        
                        
                        Rectangle obje = rectangleShape[0];
                        Graphics graphics = Graphics.FromImage(proc);
                        using (Pen border = new Pen(Color.White, 1))
                        {
                            graphics.DrawRectangle(border, obje);
                        }
                        graphics.Dispose();
                        objeX = obje.X + (obje.Width / 2); // belirlediğimiz karenin x eksenindeki ortası
                        objeY = obje.Y + (obje.Height / 2); // belirlediğimiz karenin y eksenindeki ortası

                        if (detectCoor.Checked)
                        {
                            this.Invoke((MethodInvoker)delegate {
                                //writeCoordinate.Text = obje.Location.ToString() + "\n" + writeCoordinate.Text + "\n"; // direkt olarak aforgenin özelliği
                                writeCoordinate.Text = "X: " + objeX.ToString() + " -- Y: " + objeY.ToString() + "\n" + writeCoordinate.Text + "\n"; // manuel olarka yapılan kordinat
                            });
                            
                            if (objeX > objeXValue - areaValue && objeX < objeXValue + areaValue)
                            {
                                rotation = "stop";
                            }else if (objeX >= objeXValue + areaValue)
                            {
                                rotation = "right";
                            }else if (objeX <= objeXValue - areaValue)
                            {
                                rotation = "left";
                            }
                            if (temp != rotation)
                            {
                                temp = rotation;
                                if (serialPort1.IsOpen)
                                {
                                    serialPort1.Write(rotation);
                                }
                                
                            }
                        }
                    }
                    else
                    {
                        rotation = "stop";
                        if (temp != rotation)
                        {
                            temp = rotation;
                            if (serialPort1.IsOpen)
                            {
                                serialPort1.Write(rotation);
                            }

                        }
                    }
                }
                else if (projectStatus == 3)
                {
                    string[] vs2 = cameraResolution.Split('x');
                    int objeXValue = Convert.ToInt32(vs2[0]);
                    objeXValue = objeXValue / 2;
                    Graphics graphicsCenter = Graphics.FromImage(proc);
                    Pen guidelines = new Pen(Color.Yellow, 5);
                    graphicsCenter.DrawLine(guidelines, objeXValue, 0, objeXValue, Convert.ToInt32(vs2[1]));
                    if (rectangleShape.Length > 1) // en önemli kontrol burası. Sadece iki adet renk tespiti yapılacağı için obje dizi değeri 1 den büyük olması gerekmektedir. For döngüsünü 1 e kadar yaptırıyorsak bu döngüyede 1 den büyük olduğunda girmek zorundadır.
                    {
                        for (int i=0; i<2; i++) // sadece iki adet rengi işaretlesin.
                        {
  
                            Rectangle obje = rectangleShape[i];
                            Graphics graphics = Graphics.FromImage(proc);
                            using (Pen border = new Pen(Color.White, 1))
                            {
                                graphics.DrawRectangle(border, obje);
                                graphics.DrawString((i + 1).ToString(), new Font("Arial", 12), Brushes.White, obje);
                            }
                            graphics.Dispose();
                            
                            if (rectangleShape.Length >= 1)
                            {
                                Rectangle obje1 = rectangleShape[0];
                                Rectangle obje2 = rectangleShape[1];

                                obje1x = obje1.X + (obje1.Width / 2);
                                obje1y = obje1.Y + (obje1.Height / 2);

                                obje2x = obje2.X + (obje2.Width / 2);
                                obje2y = obje2.Y + (obje2.Height / 2);
                            }
                            if (detectCoor.Checked)
                            {
                                this.Invoke((MethodInvoker)delegate {
                                    writeCoordinate.Text = "X: " + obje1x.ToString() + " -- Y: " + obje1y.ToString() + "\n" + writeCoordinate.Text + "\n"; // manuel olarka yapılan kordinat
                                    obje2Coor.Text = "X: " + obje2x.ToString() + " -- Y: " + obje2y.ToString() + "\n" + obje2Coor.Text + "\n"; // manuel olarka yapılan kordinat

                                });
                                if (serialPort1.IsOpen)
                                {
                                    serialPort1.DataReceived += new SerialDataReceivedEventHandler(ReceivedSerialHandler);
                                }
                                
                            }
                        }
                        
                    }
                }

            }
            else 
            {
                proc = getParameter;
            }
            if (captures == false)
            {
                procBox.Image = proc;
            }
            
        }
        private void divScreenProject1(int objeX, int objeY)
        {
            string[] vs = cameraResolution.Split('x');
            int xCoor = Convert.ToInt32(vs[0]);
            int yCoor = Convert.ToInt32(vs[1]);
            xCoor = xCoor / 3;
            yCoor = yCoor / 3;
            if (objeX > 0 && objeX <= xCoor && objeY > 0 && objeY <= yCoor)
            {
                panel1.BackColor = Color.FromArgb(0, 255, 0);
                panel2.BackColor = Color.FromArgb(255, 255, 255);
                panel3.BackColor = Color.FromArgb(255, 255, 255);
                panel4.BackColor = Color.FromArgb(255, 255, 255);
                panel5.BackColor = Color.FromArgb(255, 255, 255);
                panel6.BackColor = Color.FromArgb(255, 255, 255);
                panel7.BackColor = Color.FromArgb(255, 255, 255);
                panel8.BackColor = Color.FromArgb(255, 255, 255);
                panel9.BackColor = Color.FromArgb(255, 255, 255);
                ledPin = 2;

            }
            else if (objeX > xCoor && objeX <= xCoor*2 && objeY > 0 && objeY <= yCoor)
            {
                panel2.BackColor = Color.FromArgb(0, 255, 0);
                panel1.BackColor = Color.FromArgb(255, 255, 255);
                panel3.BackColor = Color.FromArgb(255, 255, 255);
                panel4.BackColor = Color.FromArgb(255, 255, 255);
                panel5.BackColor = Color.FromArgb(255, 255, 255);
                panel6.BackColor = Color.FromArgb(255, 255, 255);
                panel7.BackColor = Color.FromArgb(255, 255, 255);
                panel8.BackColor = Color.FromArgb(255, 255, 255);
                panel9.BackColor = Color.FromArgb(255, 255, 255);
                ledPin = 3;

            }
            else if (objeX > xCoor * 2 && objeX <= xCoor*3 && objeY > 0 && objeY <= yCoor)
            {
                panel3.BackColor = Color.FromArgb(0, 255, 0);
                panel1.BackColor = Color.FromArgb(255, 255, 255);
                panel2.BackColor = Color.FromArgb(255, 255, 255);
                panel4.BackColor = Color.FromArgb(255, 255, 255);
                panel5.BackColor = Color.FromArgb(255, 255, 255);
                panel6.BackColor = Color.FromArgb(255, 255, 255);
                panel7.BackColor = Color.FromArgb(255, 255, 255);
                panel8.BackColor = Color.FromArgb(255, 255, 255);
                panel9.BackColor = Color.FromArgb(255, 255, 255);
                ledPin = 4;
            }
            else if (objeX > 0 && objeX <= xCoor && objeY > yCoor && objeY <= yCoor*2)
            {
                panel4.BackColor = Color.FromArgb(0, 255, 0);
                panel1.BackColor = Color.FromArgb(255, 255, 255);
                panel2.BackColor = Color.FromArgb(255, 255, 255);
                panel3.BackColor = Color.FromArgb(255, 255, 255);
                panel5.BackColor = Color.FromArgb(255, 255, 255);
                panel6.BackColor = Color.FromArgb(255, 255, 255);
                panel7.BackColor = Color.FromArgb(255, 255, 255);
                panel8.BackColor = Color.FromArgb(255, 255, 255);
                panel9.BackColor = Color.FromArgb(255, 255, 255);
                ledPin = 5;
            }
            else if (objeX > xCoor && objeX <= xCoor*2 && objeY > yCoor && objeY <= yCoor*2)
            {
                panel5.BackColor = Color.FromArgb(0, 255, 0);
                panel1.BackColor = Color.FromArgb(255, 255, 255);
                panel2.BackColor = Color.FromArgb(255, 255, 255);
                panel3.BackColor = Color.FromArgb(255, 255, 255);
                panel4.BackColor = Color.FromArgb(255, 255, 255);
                panel6.BackColor = Color.FromArgb(255, 255, 255);
                panel7.BackColor = Color.FromArgb(255, 255, 255);
                panel8.BackColor = Color.FromArgb(255, 255, 255);
                panel9.BackColor = Color.FromArgb(255, 255, 255);
                ledPin = 6;
            }
            else if (objeX > xCoor*2 && objeX <= xCoor*3 && objeY > yCoor && objeY <= yCoor*2)
            {
                panel6.BackColor = Color.FromArgb(0, 255, 0);
                panel1.BackColor = Color.FromArgb(255, 255, 255);
                panel2.BackColor = Color.FromArgb(255, 255, 255);
                panel3.BackColor = Color.FromArgb(255, 255, 255);
                panel4.BackColor = Color.FromArgb(255, 255, 255);
                panel5.BackColor = Color.FromArgb(255, 255, 255);
                panel7.BackColor = Color.FromArgb(255, 255, 255);
                panel8.BackColor = Color.FromArgb(255, 255, 255);
                panel9.BackColor = Color.FromArgb(255, 255, 255);
                ledPin = 7;
            }
            else if (objeX > 0 && objeX <= xCoor && objeY > yCoor * 2 && objeY <= yCoor*3)
            {
                panel7.BackColor = Color.FromArgb(0, 255, 0);
                panel1.BackColor = Color.FromArgb(255, 255, 255);
                panel2.BackColor = Color.FromArgb(255, 255, 255);
                panel3.BackColor = Color.FromArgb(255, 255, 255);
                panel4.BackColor = Color.FromArgb(255, 255, 255);
                panel5.BackColor = Color.FromArgb(255, 255, 255);
                panel6.BackColor = Color.FromArgb(255, 255, 255);
                panel8.BackColor = Color.FromArgb(255, 255, 255);
                panel9.BackColor = Color.FromArgb(255, 255, 255);
                ledPin = 8;
            }
            else if (objeX > xCoor && objeX <= xCoor*2 && objeY > yCoor * 2 && objeY <= yCoor*3)
            {
                panel8.BackColor = Color.FromArgb(0, 255, 0);
                panel1.BackColor = Color.FromArgb(255, 255, 255);
                panel2.BackColor = Color.FromArgb(255, 255, 255);
                panel3.BackColor = Color.FromArgb(255, 255, 255);
                panel4.BackColor = Color.FromArgb(255, 255, 255);
                panel5.BackColor = Color.FromArgb(255, 255, 255);
                panel6.BackColor = Color.FromArgb(255, 255, 255);
                panel7.BackColor = Color.FromArgb(255, 255, 255);
                panel9.BackColor = Color.FromArgb(255, 255, 255);
                ledPin = 9;
            }
            else if (objeX > xCoor*2 && objeX <= xCoor*3 && objeY > yCoor * 2 && objeY <= yCoor*3)
            {
                panel9.BackColor = Color.FromArgb(0, 255, 0);
                panel1.BackColor = Color.FromArgb(255, 255, 255);
                panel2.BackColor = Color.FromArgb(255, 255, 255);
                panel3.BackColor = Color.FromArgb(255, 255, 255);
                panel4.BackColor = Color.FromArgb(255, 255, 255);
                panel5.BackColor = Color.FromArgb(255, 255, 255);
                panel6.BackColor = Color.FromArgb(255, 255, 255);
                panel7.BackColor = Color.FromArgb(255, 255, 255);
                panel8.BackColor = Color.FromArgb(255, 255, 255);
                ledPin = 10;
            }
        }
        private void ReceivedSerialHandler(object sender, SerialDataReceivedEventArgs e)
        {
                if (serialPort1.IsOpen)
                {
                    SerialPort getdataPort = (SerialPort)sender;
                    if (projectStatus == 1)
                    {
                        if (getdataPort.ReadExisting() == "Project1Recived")
                        {
                            serialPort1.Write(ledPin.ToString());
                        }
                    }else if (projectStatus == 3)
                    {
                        if (getdataPort.ReadExisting() == "Project3Recived")
                        {
                            serialPort1.Write(obje1y + "*" + obje2y);
                        }
                    }
                }
            else
                {
                    serialPort1.Open();
                }
            
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            selectPort.Items.Clear();
            selectPort.Items.AddRange(ports);
            disconnectSerial.Enabled = false;
            proje1ToolStripMenuItem.Checked = true;
            vGAToolStripMenuItem.Checked = true;
            middleArea.Visible = false;
            obje2.Visible = false;
        }

        private void stop_Click(object sender, EventArgs e)
        {
            if (camera.IsRunning)
            {
                camera.Stop();
            }
            start.Enabled = true;
            stop.Enabled = false;
            capture.Enabled = false;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            detectCoor.CheckState = CheckState.Unchecked;
            Process.GetCurrentProcess().Kill(); // zorla kapatma
            if (cameraRun == 1)
            {
                camera.Stop();
            }
        }

        private void capture_Click(object sender, EventArgs e)
        {
            if (captures == true)
            {
                captures = false;
            }else if (captures == false)
            {
                captures = true;
            }
        }

        private void redTrack_Scroll(object sender, EventArgs e)
        {
            R = redTrack.Value;
        }

        private void greenTrack_Scroll(object sender, EventArgs e)
        {
            G = greenTrack.Value;
        }

        private void blueTrack_Scroll_1(object sender, EventArgs e)
        {
            B = blueTrack.Value;
        }

        private void connectSerial_Click(object sender, EventArgs e)
        {
            if (selectPort.Text != "" && selectBaudRate.Text != "")
            {
                serialPort1.PortName = selectPort.Text;
                serialPort1.BaudRate = Convert.ToInt32(selectBaudRate.Text);
                serialPort1.Parity = Parity.None;
                serialPort1.StopBits = StopBits.One;
                serialPort1.Open();
                connectTrack.Value = 100;
                connectSerial.Enabled = false;
                disconnectSerial.Enabled = true;
               
            }
            else
            {
                MessageBox.Show("Eksik ayar bıraktınız!","Uyarı", MessageBoxButtons.OK,MessageBoxIcon.Warning);
                
            }
            
        }

        private void disconnectSerial_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Close();
                connectTrack.Value = 0;
                connectSerial.Enabled = true;
                disconnectSerial.Enabled = false;
                detectDevices.Enabled = false;
            }
            
        }

        private void detectDevices_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            selectPort.Items.Clear();
            selectPort.Items.AddRange(ports);
            MessageBox.Show("Kontrol sağlandı bulunan tüm cihazlar listelendi.", "Bilgilendirme", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void görüntüyüAynalaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (mirroring == 0)
            {
                mirroring = 1;
                görüntüyüAynalaToolStripMenuItem.Checked = true;
            }
            else if (mirroring == 1)
            {
                mirroring = 0;
                görüntüyüAynalaToolStripMenuItem.Checked = false;
            }
        }

        private void ardiunoProject1_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Write("Proje1");
            }
            else
            {
                MessageBox.Show("Seri port bağlantısı sağlanmadan bu özellik kullanılamaz.", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void ardiunoProject2_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Write("Proje2");
            }
            else
            {
                MessageBox.Show("Seri port bağlantısı sağlanmadan bu özellik kullanılamaz.", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void ardiunoProject3_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Write("Proje3");
            }
            else
            {
                MessageBox.Show("Seri port bağlantısı sağlanmadan bu özellik kullanılamaz.", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void proje2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            projectStatus = 2;
            projeStatusViewer.Text = "Proje 2";
            proje1ToolStripMenuItem.Checked = false;
            proje2ToolStripMenuItem.Checked = true;
            proje3ToolStripMenuItem.Checked = false;
            ledinterface.Visible = false;
            obje2.Visible = false;
            middleArea.Visible = true;
            ardiunoProject1.Enabled = false;
            ardiunoProject2.Enabled = true;
            ardiunoProject3.Enabled = false;
        }

        private void vGAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cameraResolution = "640x480";
            ItemClick(vGAToolStripMenuItem);
            
        }

        private void mP1280x960ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cameraResolution = "1280x960";
            ItemClick(mP1280x960ToolStripMenuItem);
        }

        private void mP2048x1536ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cameraResolution = "2048x1536";
            ItemClick(mP2048x1536ToolStripMenuItem);
        }

        private void mP1600x1200ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cameraResolution = "1600x1200";
            ItemClick(mP1600x1200ToolStripMenuItem);
        }

        private void mP2240x1680ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cameraResolution = "2240x1680";
            ItemClick(mP2240x1680ToolStripMenuItem);
        }

        private void mP2560x1920ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cameraResolution = "2560x1920";
            ItemClick(mP2560x1920ToolStripMenuItem);
        }

        private void mP3032x2008ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cameraResolution = "3032x2008";
            ItemClick(mP3032x2008ToolStripMenuItem);
        }

        private void mP3072x2304ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cameraResolution = "3072x2304";
            ItemClick(mP3072x2304ToolStripMenuItem);
        }

        private void mP3264x2448ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cameraResolution = "3264x2448";
            ItemClick(mP3264x2448ToolStripMenuItem);
        }

        private void kaydetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "PNG|*.png";
            DialogResult result = saveFileDialog1.ShowDialog();
            ImageFormat format = ImageFormat.Png;

            if (result == DialogResult.OK)
            {
                procBox.Image.Save(saveFileDialog1.FileName, format);
            }
        }

        private void areaTrackBar_Scroll(object sender, EventArgs e)
        {
            areaValueView.Text = areaTrackBar.Value.ToString();
            areaValue = areaTrackBar.Value;
        }

        private void proje3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            projectStatus = 3;
            projeStatusViewer.Text = "Proje 3";
            proje1ToolStripMenuItem.Checked = false;
            proje2ToolStripMenuItem.Checked = false;
            proje3ToolStripMenuItem.Checked = true;
            ledinterface.Visible = false;
            
            ardiunoProject1.Enabled = false;
            ardiunoProject2.Enabled = false;
            ardiunoProject3.Enabled = true;
            middleArea.Visible = false;
            obje2.Visible = true;
            obje2Coor.Visible = true;
        }

        private void proje1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            projectStatus = 1;
            projeStatusViewer.Text = "Proje 1";
            proje1ToolStripMenuItem.Checked = true;
            proje2ToolStripMenuItem.Checked = false;
            proje3ToolStripMenuItem.Checked = false;
            ledinterface.Visible = true;
            obje2.Visible = false;
            ardiunoProject1.Enabled = true;
            ardiunoProject2.Enabled = false;
            ardiunoProject3.Enabled = false;
            middleArea.Visible = false;


        }
        private void ItemClick(object sender)
        {
            foreach (ToolStripMenuItem item in kameraÇözünürlüğüToolStripMenuItem.DropDownItems)
            {
                if (item != sender)
                {
                    item.Checked = false;
                }
                else
                {
                    item.Checked = true;
                    resolutionView.Text = cameraResolution;
                }
            }
        }
    }
}
